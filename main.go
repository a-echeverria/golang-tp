package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func createEntriesFile() []byte {
	f, err := os.OpenFile("entries.data", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		fmt.Print(err)
	}

	_, err = f.Write([]byte(""))
	if err != nil {
		fmt.Print(err)
	}
	f.Close()
	return []byte("")
}

func readEntriesFile() []string {
	entries, err := ioutil.ReadFile("entries.data")
	if err != nil {
		entries = createEntriesFile()
	}
	oldEntries := strings.Split(string(entries), "\n")
	return oldEntries
}

func timeHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		t := time.Now()
		fmt.Fprintf(w, "%vh%v", strconv.Itoa(t.Hour()), strconv.Itoa(t.Minute()))
	} else {
		fmt.Println("Method is not allowed.")
		fmt.Fprintln(w, "Method is not allowed.")
	}
}

func addHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}

		for key := range req.PostForm {
			if key != "entry" && key != "author" {
				fmt.Printf("Invalid key %v!\n", key)
				fmt.Fprintf(w, "Invalid key %v!\n", key)
				return
			}
		}

		entries := readEntriesFile()
		var newEntries string
		for i := 0; i < len(entries)-1; i++ {
			newEntries = newEntries + entries[i] + "\n"
		}
		newEntries = newEntries + req.FormValue("author") + ": " + req.FormValue("entry") + "\n"
		entriesToSave := []byte(newEntries)
		err := ioutil.WriteFile("entries.data", entriesToSave, 0777)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		fmt.Println("Method is not allowed.")
		fmt.Fprintln(w, "Method is not allowed.")
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		entries := readEntriesFile()
		var entriesToReturn string
		for i := 0; i < len(entries)-1; i++ {
			entrySplitted := strings.Split(entries[i], ": ")
			entriesToReturn = entriesToReturn + entrySplitted[1] + "\n"
		}
		fmt.Fprint(w, entriesToReturn)
	} else {
		fmt.Println("Method is not allowed.")
		fmt.Fprintln(w, "Method is not allowed.")
	}
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	fmt.Print("Listening on port 4567\n")
	http.ListenAndServe(":4567", nil)
}
